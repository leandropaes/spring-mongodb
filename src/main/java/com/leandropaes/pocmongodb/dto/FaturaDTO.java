package com.leandropaes.pocmongodb.dto;

import com.leandropaes.pocmongodb.enums.StatusEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FaturaDTO {
    private String id;
    private String correlationId;
    private Integer idConta;
    private Integer numeroFatura;
    private String referencia;
    private Double valorTotal;
    private StatusEnum status;
}
