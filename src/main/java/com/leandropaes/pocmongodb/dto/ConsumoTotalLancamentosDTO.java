package com.leandropaes.pocmongodb.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConsumoTotalLancamentosDTO {
    private String id;
    private Integer total;
    private Integer qtd_pedagio;
    private Integer qtd_valePedagio;
    private Integer qtd_estacionamento;
    private Integer qtd_abastecimento;
}
