package com.leandropaes.pocmongodb.service;

import com.leandropaes.pocmongodb.dto.ConsumoTotalLancamentosDTO;
import com.leandropaes.pocmongodb.dto.FaturaDTO;
import com.leandropaes.pocmongodb.model.Consumo;
import com.leandropaes.pocmongodb.model.Fatura;
import com.leandropaes.pocmongodb.model.ServicoVeloe;
import com.leandropaes.pocmongodb.repository.ConsumoRepository;
import com.leandropaes.pocmongodb.repository.FaturaRepository;
import com.leandropaes.pocmongodb.repository.ServicoVeloeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class FaturaService {

    private final FaturaRepository faturaRepository;
    private final ServicoVeloeRepository servicoVeloeRepository;
    private final ConsumoRepository consumoRepository;
    private final MongoTemplate mongoTemplate;

    public Fatura adicionaSaldo(Integer numeroFatura) {
        Query query = new Query(Criteria.where("numeroFatura").is(numeroFatura));
        Update update = new Update().inc("valorTotal", 50.0);

        return mongoTemplate.update(Fatura.class)
                .matching(query)
                .apply(update)
                .withOptions(FindAndModifyOptions.options().returnNew(true))
                .findAndModifyValue();
    }

    public List<FaturaDTO> all() {
        final Query query = new Query();
        query.fields().include("correlationId", "numeroFatura", "idConta", "status", "referencia", "valorTotal");

        return mongoTemplate.find(query, Fatura.class)
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    public ConsumoTotalLancamentosDTO totalLancamentos(Integer numeroFatura) {
        Criteria criteria = new Criteria("numeroFatura").is(numeroFatura);
        MatchOperation matchStage = Aggregation.match(criteria);

        ConditionalOperators.Cond condPedagio = getSizeFieldOrZero("pedagio.itens");
        ConditionalOperators.Cond condValePedagio = getSizeFieldOrZero("valePedagio.itens");
        ConditionalOperators.Cond condEstacionamento = getSizeFieldOrZero("estacionamento.itens");
        ConditionalOperators.Cond condAbastecimento = getSizeFieldOrZero("abastecimento.itens");

        ArithmeticOperators.Add addOperator = ArithmeticOperators.Add
                .valueOf(condPedagio)
                .add(condValePedagio)
                .add(condEstacionamento)
                .add(condAbastecimento);

        GroupOperation group = Aggregation
                .group("numeroFatura")
                .sum(addOperator).as("total");
//                .sum(condPedagio).as("qtd_pedagio")
//                .sum(condValePedagio).as("qtd_valePedagio")
//                .sum(condEstacionamento).as("qtd_estacionamento")
//                .sum(condAbastecimento).as("qtd_abastecimento");

        Aggregation aggregation = Aggregation.newAggregation(matchStage, group);

        return mongoTemplate.aggregate(aggregation, "consumos", ConsumoTotalLancamentosDTO.class)
                .getUniqueMappedResult();
    }

    private ConditionalOperators.Cond getSizeFieldOrZero(String field) {
        return ConditionalOperators.when(ArrayOperators.IsArray.isArray(field))
                .thenValueOf(ArrayOperators.Size.lengthOfArray(field))
                .otherwise(0);
    }

    public Fatura find(String id) {
        return faturaRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Fatura não existe"));
    }

    public List<FaturaDTO> findByNumeroFatura(Integer numeroFatura) {
        return faturaRepository
                .findFaturaByNumeroFatura(numeroFatura)
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    public FaturaDTO store(Fatura fatura) {
        ServicoVeloe servicoVeloe = storeServicosVeloe(fatura);
        List<Consumo> consumos = storeConsumos(fatura);

        fatura.setServicosVeloe(servicoVeloe);
        fatura.setConsumos(consumos);

        Fatura faturaSaved = faturaRepository.save(fatura);
        return convertToDTO(faturaSaved);
    }

    private ServicoVeloe storeServicosVeloe(Fatura fatura) {
        ServicoVeloe servicosVeloe = fatura.getServicosVeloe();
        servicosVeloe.setNumeroFatura(fatura.getNumeroFatura());
        ServicoVeloe servicoVeloe = servicoVeloeRepository.save(servicosVeloe);
        return servicoVeloe;
    }

    private List<Consumo> storeConsumos(Fatura fatura) {
        List<Consumo> consumos = new ArrayList<>();

        fatura.getConsumos().forEach(consumo -> {
            consumo.setNumeroFatura(fatura.getNumeroFatura());
            Consumo consumoSaved = consumoRepository.save(consumo);
            consumos.add(consumoSaved);
        });

        return consumos;
    }

    private FaturaDTO convertToDTO(Fatura fatura) {
        return FaturaDTO.builder()
                .id(fatura.getId())
                .correlationId(fatura.getCorrelationId())
                .idConta(fatura.getIdConta())
                .numeroFatura(fatura.getNumeroFatura())
                .referencia(fatura.getReferencia())
                .valorTotal(fatura.getValorTotal())
                .status(fatura.getStatus())
                .build();
    }
}
