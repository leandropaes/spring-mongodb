package com.leandropaes.pocmongodb.enums;

public enum StatusEnum {
    ABERTA,
    EM_FECHAMENTO,
    FECHADA,
    CONFLITO
}
