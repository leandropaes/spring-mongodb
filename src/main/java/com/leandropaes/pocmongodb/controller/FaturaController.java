package com.leandropaes.pocmongodb.controller;

import com.leandropaes.pocmongodb.dto.ConsumoTotalLancamentosDTO;
import com.leandropaes.pocmongodb.dto.FaturaDTO;
import com.leandropaes.pocmongodb.model.Consumo;
import com.leandropaes.pocmongodb.model.Fatura;
import com.leandropaes.pocmongodb.service.FaturaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/faturas")
@RequiredArgsConstructor
public class FaturaController {

    private final FaturaService faturaService;

    @GetMapping
    public ResponseEntity<List<FaturaDTO>> list(
            @RequestParam(required = false) Integer numeroFatura
    ) {
        if (numeroFatura != null) {
            return ResponseEntity.ok(faturaService.findByNumeroFatura(numeroFatura));
        }

        return ResponseEntity.ok(faturaService.all());
    }

    @GetMapping("/total/{numeroFatura}")
    public ConsumoTotalLancamentosDTO totalLancamentos(@PathVariable Integer numeroFatura) {
        return faturaService.totalLancamentos(numeroFatura);
    }

    @GetMapping("/inc/{numeroFatura}")
    public ResponseEntity<Fatura> adicionaSaldo(@PathVariable Integer numeroFatura) {
        return ResponseEntity.ok(faturaService.adicionaSaldo(numeroFatura));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Fatura> find(@PathVariable String id) {
        return ResponseEntity.ok(faturaService.find(id));
    }

    @PostMapping
    public ResponseEntity<FaturaDTO> store(@RequestBody Fatura fatura) {
        return new ResponseEntity<>(faturaService.store(fatura), HttpStatus.CREATED);
    }
}
