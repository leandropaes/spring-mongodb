package com.leandropaes.pocmongodb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EstacionamentoDetalhe {
    private LocalDateTime entrada;
    private LocalDateTime saida;
    private String estabelecimento;
    private String permanencia;
    private Double valor;
}
