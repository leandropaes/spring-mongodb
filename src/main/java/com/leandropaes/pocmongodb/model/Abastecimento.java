package com.leandropaes.pocmongodb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Abastecimento {
    private String descricao;
    private Double total;
    private List<AbastecimentoDetalhe> itens;
}
