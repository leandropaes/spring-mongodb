package com.leandropaes.pocmongodb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document("servicosVeloe")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ServicoVeloe {
    @Id
    private String id;
    private Integer numeroFatura;
    private Double total;
    private List<ServicoVeloeDetalhe> itens;
}
