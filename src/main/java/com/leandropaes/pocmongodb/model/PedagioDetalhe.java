package com.leandropaes.pocmongodb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PedagioDetalhe {
    private LocalDateTime data;
    private String estabelecimento;
    private String categoriaCobrada;
    private Double valor;
}
