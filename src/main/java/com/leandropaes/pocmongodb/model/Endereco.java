package com.leandropaes.pocmongodb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Endereco {
    private String logradouro;
    private String complemento;
    private String bairro;
    private String cep;
    private String cidade;
    private String estado;
}
