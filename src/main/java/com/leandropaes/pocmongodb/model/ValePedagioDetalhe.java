package com.leandropaes.pocmongodb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ValePedagioDetalhe {
    private LocalDateTime data;
    private String numeroViagem;
    private String estabelecimento;
    private String categoriaCobrada;
    private Double valor;
}
