package com.leandropaes.pocmongodb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AbastecimentoDetalhe {
    private LocalDateTime data;
    private String descricao;
    private String bonificacao;
    private Double valor;
}
