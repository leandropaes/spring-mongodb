package com.leandropaes.pocmongodb.model;

import com.leandropaes.pocmongodb.enums.StatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Document("faturas")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Fatura {
    @Id
    private String id;
    private String correlationId;
    private Integer idConta;
    private Integer numeroFatura;
    private String notaFiscal;
    private String linkNotaFiscal;
    private LocalDateTime dataFechamento;
    private LocalDate dataVencimento;
    private String referencia;
    private String descricaoPlano;
    private Double valorTotal;
    private Integer totalVeiculos;
    private Empresa empresa;
    private Cliente cliente;
    private StatusEnum status;
    private Header header;
    @DBRef
    private ServicoVeloe servicosVeloe;
    @DBRef
    private List<Consumo> consumos;
}
