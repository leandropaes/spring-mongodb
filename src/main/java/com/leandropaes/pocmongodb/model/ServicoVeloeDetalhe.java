package com.leandropaes.pocmongodb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ServicoVeloeDetalhe {
    private String descricao;
    private LocalDateTime data;
    private Double valor;
}
