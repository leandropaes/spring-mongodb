package com.leandropaes.pocmongodb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("consumos")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Consumo {
    @Id
    private String id;
    private Integer numeroFatura;
    private String placa;
    private String categoria;
    private Double total;
    private Pedagio pedagio;
    private ValePedagio valePedagio;
    private Estacionamento estacionamento;
    private Abastecimento abastecimento;
}
