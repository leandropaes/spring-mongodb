package com.leandropaes.pocmongodb.repository;

import com.leandropaes.pocmongodb.model.Consumo;
import com.leandropaes.pocmongodb.model.ServicoVeloe;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServicoVeloeRepository extends MongoRepository<ServicoVeloe, String> {

}
