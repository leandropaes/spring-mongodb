package com.leandropaes.pocmongodb.repository;

import com.leandropaes.pocmongodb.model.Consumo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConsumoRepository extends MongoRepository<Consumo, String> {

}
