package com.leandropaes.pocmongodb.repository;

import com.leandropaes.pocmongodb.model.Fatura;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FaturaRepository extends MongoRepository<Fatura, String> {
    @Query(
        value = "{ 'numeroFatura': ?0 }",
        fields = "{ 'id': 1, 'correlationId': 1, 'idConta': 1, 'numeroFatura': 1, 'referencia': 1, 'valorTotal': 1, 'status': 1 }"
    )
    List<Fatura> findFaturaByNumeroFatura(Integer numeroFatura);
}
