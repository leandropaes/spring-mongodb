#!/bin/bash

FILE=$(cat payload.json)

for i in {1010..1001};
  do
    CONTENT=$(echo $FILE | sed -e "/99999999999999/ s//$i/g")
    CONTENT=$(echo $CONTENT | sed -e "/##CORRELATION_ID##/ s//$i/g")
    echo $CONTENT > /tmp/contenttemp.json
    curl --location --request POST 'http://localhost:8080/faturas' --header 'Content-Type: application/json' --data @/tmp/contenttemp.json
    echo
  done
